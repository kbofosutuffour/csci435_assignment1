# CSCI435_Assignment1: Project Description

Kwaku Ofosu-Tuffour

CSCI 435: Software Engineering

Fall 2023


The purpose of this project was to interpret xml files, and highlight the leaf nodes in each of the images that represent each xml file.  The input directory contains both the xml files and the png images that represent each file.  The output directory contains annotated screenshots of the edited images of the input photos.  This project runs using a Jupyter notebook (main.ipynb).  The code uses the OpenCV python library, which is an open source computer vision and machine learning software.  The library is used to load images in the input directory, create copies of each photo, and edit each photo based on the xml layouts in the input directory.  I used Python Jupyter Notebooks due to its simplicity in compiling all aspects of the project in one lcoation, and I used the OpenCV library as it is commonly used for editing photos with python (and C++) based on research.  For more information on OpenCV, please reference the official documentation here: [https://opencv-python.readthedocs.io/_/downloads/en/latest/pdf/](url).

The Jupyter notebook main file (main.ipynb) includes various defined functions to complete the task.  The code includes functions to read and return copies of images, functions to parse through each node of the xml file, functions to extract the _bounds_ and _class_ attributes of each node to find the leaf nodes, and finally functions to edit the copy of each image.  For more information, each function in the main.ipynb includes documentation that explains their purpose.

To run the main file, please make sure to use a Jupyter notebook, and make sure to have the OpenCV library installed: [https://pypi.org/project/opencv-python/](url) or use the command **pip install opencv-python**.
